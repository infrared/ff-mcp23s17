# Copyright and Licensing

[![REUSE status](https://api.reuse.software/badge/codeberg.org/infrared/ff-mcp23s17)](https://api.reuse.software/info/codeberg.org/infrared/ff-mcp23s17)

Readme.md is part of the ff-mcp23s17 project.

ff-mcp23s17 is a project to control the MCP23S17 chip with FlashForth.

Copyright (C) 2021  Christopher Howard

SPDX-License-Identifier: GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Description

This project contains FlashForth code to interface between the
ATmega328P microcontroller and the MCP23S17 16-Bit I/O Expander IC,
using SPI communication.

# Requirements

ATmega328P microcontroller connected to a MCP23S17 chip over SPI
lines. An Arduino Nano 3.0 was used for development.

The code was developed for this FlashForth build:

FlashForth 5 ATmega328 18.11.2020

You will need the `doloop.fs` file, available in the `avr/forth`
directory of the FlashForth source code. It is not included in the
ff-ad9833 source since it is part of the FlashForth system.

# Compiling

After setting up a FlashForth Shell connection, run these commands in
the Shell.

In FlashForth, you only need to compile (#send) the code once, and
then you can run the compiled words later, even after resetting the
microcontroller. For more information about FlashForth, visit

[FlashForth Web Site](https://flashforth.com/)

## Core words and definitions for controlling the MCP23S17

    #send doloop
    #send util
    #send 328p
    #send pin
    #send 328p-pins
    #send mcp23s17

## Demo words

    #send mcp23s17-demos

# Use

See the source code in `mcp23s17-demos.fs` for examples.

# Running Demos

Enter the word `words` and look for words prefixed with `demo-`.

# Code documentation

Each Forth code file has the following documentation in comments:
1. The title of the file
2. A copyright and license header
3. A description of the purpose of the file
4. A list of public words, i.e., words meant to be used outside of the
module itself
5. The code is divided into sections, each containing a section
description.
6. Each dictionary word, not including words like constants and
variables, contains a brief Forth-style stack effect description.

# Contact

Christopher Howard

e-mail address in Base64 encoding:

    Y2hyaXN0b3BoZXIgLUFULSBsaWJyZWhhY2tlciAtRE9ULSBjb20K