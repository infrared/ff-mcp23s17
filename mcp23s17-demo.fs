\ **********************************************************************
\
\ mcp23s17-demo.fs is part of the ff-mcp23s17 project
\ ff-mcp23s17 is a project to intract with the MCP23S17-DEMO GPIO expander
\ using FlashForth
\ Copyright (C) 2021  Christopher Howard
\
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ This program is free software: you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation, either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <https://www.gnu.org/licenses/>.
\
\ This module provides demo code for interacting with the MCP23S17
\ public words:
\   demo-gpioa-out demo-gpioa-in demo-repeat-in
\
\ **********************************************************************

mcp23s17-demo
marker mcp23s17-demo

\ **********************************************************************
\ demo-gpioa-out: sets the GPIOA pins to 10101010. Assumes default
\ MCP23S17 settings.
\ **********************************************************************

: demo-gpioa-out ( -- )
    PD2 init-output-pin init-spi PD2 pin-low
    WRITE_BYTE tx-spi B0_IODIRA tx-spi 0 tx-spi
    PD2 pin-high 0 drop PD2 pin-low
    WRITE_BYTE tx-spi B0_GPIOA tx-spi %10101010 tx-spi
    PD2 pin-high
;

\ **********************************************************************
\ demo-gpioa-in: pulls the bits from GPIOA and drops them on the stack
\ as a byte. Assumes default MCP23S17 settings.
\ **********************************************************************

: demo-gpioa-in ( -- c )
    PD2 init-output-pin init-spi PD2 pin-low
    WRITE_BYTE tx-spi B0_IODIRA tx-spi %11111111 tx-spi
    PD2 pin-high 0 drop PD2 pin-low
    READ_BYTE tx-spi B0_GPIOA tx-spi 0 tx-spi rx-spi
    PD2 pin-high
;

\ **********************************************************************
\ demo-repeat-in: Once every second, pulls the 8 bits from GPIOA and
\ displays them on serial output. Loops until you reset the MC.
\ **********************************************************************

: demo-repeat-in
    PD2 init-output-pin init-spi PD2 pin-low
    WRITE_BYTE tx-spi B0_IODIRA tx-spi %11111111 tx-spi
    PD2 pin-high 0 drop PD2 pin-low
    WRITE_BYTE tx-spi B0_IOCON tx-spi
    [ 1 BANK_BT lshift 1 SEQOP_BT lshift and ] tx-spi
    PD2 pin-high 0 drop PD2 pin-low
    READ_BYTE tx-spi B1_GPIOA tx-spi
    bin begin 0 tx-spi rx-spi . 1000 ms again
; 