\ **********************************************************************

\ ff-328p.fs - Atmega328P definitions and communications

\ Copyright 2020, 2021 Christopher Howard

\ SPDX-License-Identifier: GPL-3.0-or-later

\ This program is free software: you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation, either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <https://www.gnu.org/licenses/>.

\ The purpose of this module is to provide
\ 1. 328P register and port definitions
\ 2. words to assist in MC communications (e.g., SPI)

\ Reference the ATmega328P data sheet for the meanings of the of the
\ register and port definitions. It is available at

\ https://ww1.microchip.com/downloads/en/DeviceDoc/
\   Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf

\ Public words:
\   PINB PINC PIND PORTB PORTC PORTD DDRB DD_OUT DD_IN DDRD DDR_SPI
\   DD_SS DD_MOSI DD_SCK SPCR SPR0 SPR1 CPHA CPHA_SMPTRL CPHA_SMPLED
\   CPOL CPOL_HIDLE CPOL_LIDLE MSTR MSTR_MSTR MSTR_SLAVE DORD DORD_LSB
\   DORD_MSB SPE SPE_ENAB SPE_DISAB SPIE SPIE_ENAB SPIE_DISAB SPSR SPI2X
\   WCOL SPIF SPDR tx-spi 2tx-spi rx-spi

\ **********************************************************************

328p
marker 328p

\ **********************************************************************
\ Input port addresses
\ **********************************************************************

$23 constant PINB
$26 constant PINC
$29 constant PIND

\ **********************************************************************
\ Output port addresses
\ **********************************************************************

$25 constant PORTB
$28 constant PORTC
$2b constant PORTD

\ **********************************************************************
\ Data Direction Registers
\ **********************************************************************

$24 constant DDRB
  1 constant DD_OUT
  0 constant DD_IN
$27 constant DDRC
$2a constant DDRD

\ **********************************************************************
\ DDR for SPI comms
\ **********************************************************************

$24 constant DDR_SPI
$2 constant DD_SS
$3 constant DD_MOSI
$5 constant DD_SCK

\ **********************************************************************
\ SPI control register
\ **********************************************************************

$4c constant SPCR
$0 constant SPR0
$1 constant SPR1
$2 constant CPHA
  1 constant CPHA_SMPTRL
  0 constant CPHA_SMPLED
$3 constant CPOL
  1 constant CPOL_HIDLE
  0 constant CPOL_LIDLE
$4 constant MSTR
  1 constant MSTR_MSTR
  0 constant MSTR_SLAVE
$5 constant DORD
  1 constant DORD_LSB
  0 constant DORD_MSB
$6 constant SPE
  1 constant SPE_ENAB
  0 constant SPE_DISAB
$7 constant SPIE
  1 constant SPIE_ENAB
  0 constant SPIE_DISAB

\ **********************************************************************
\ SPI status register
\ **********************************************************************

$4d constant SPSR
$0 constant SPI2X
$6 constant WCOL
$7 constant SPIF

\ **********************************************************************
\ Transmitting data over SPI. Note that `2tx-spi-msb` assumes you have
\ properly initialized SPI for MSB transmission.
\ **********************************************************************

$4e constant SPDR

: tx-spi ( c -- )
    SPDR c! begin SPSR c@ 1 SPIF lshift and until
;

: 2tx-spi-msb ( u -- ) dup high-byte tx-spi low-byte tx-spi ;

\ **********************************************************************
\ rx-spi: wait until no transmission is in progress and then read byte
\ from SPI buffer. N.B. that this does not actually cause the rx to
\ happen, it just gets the value from the register. You must tx a byte
\ to rx a byte, since this is SPI
\ **********************************************************************

: rx-spi ( -- c ) begin SPSR c@ 1 SPIF lshift and until SPDR c@ ;
