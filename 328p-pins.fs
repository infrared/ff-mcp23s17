\ **********************************************************************
\
\ 328p-pins.fs is part of the ff-mcp23s17 project
\ ff-mcp23s17 is a project to interact with the MCP23S17 GPIO expander
\ using FlashForth
\ Copyright (C) 2021  Christopher Howard
\
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ This program is free software: you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation, either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <https://www.gnu.org/licenses/>.
\
\ The purpose of this module is to provide pin objects for the
\ ATmega328P.
\
\ public words:
\   PD2 PD3 PD4 PD5 PD6 PD7 PB0 PB1 PB2 PC0 PC1 PC2 PC3 PC4 PC5
\
\ **********************************************************************

328p-pins
marker 328p-pins

\ **********************************************************************
\ These are gpio-pin objects covering most of the commonly used digital
\ pins.
\ **********************************************************************

flash
DDRD PIND PORTD 2 gpio-pin PD2
DDRD PIND PORTD 3 gpio-pin PD3
DDRD PIND PORTD 4 gpio-pin PD4
DDRD PIND PORTD 5 gpio-pin PD5
DDRD PIND PORTD 6 gpio-pin PD6
DDRD PIND PORTD 7 gpio-pin PD7
DDRB PINB PORTB 0 gpio-pin PB0
DDRB PINB PORTB 1 gpio-pin PB1
DDRB PINB PORTB 2 gpio-pin PB2
DDRC PINC PORTC 0 gpio-pin PC0
DDRC PINC PORTC 1 gpio-pin PC1
DDRC PINC PORTC 2 gpio-pin PC2
DDRC PINC PORTC 3 gpio-pin PC3
DDRC PINC PORTC 4 gpio-pin PC4
DDRC PINC PORTC 5 gpio-pin PC5
ram
