\ **********************************************************************
\
\ pin.fs is part of the ff-mcp23s17 project
\ ff-mcp23s17 is a project to intract with the MCP23S17 GPIO expander
\ using FlashForth
\ Copyright (C) 2021  Christopher Howard
\
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ This program is free software: you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation, either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <https://www.gnu.org/licenses/>.
\
\ This module provides an interface for interacting with AVR pins
\
\ public words:
\
\ **********************************************************************

pin
marker pin

: get-ddr-port ( gpio-pin -- ddr ) 3 cells + @ ;

: get-in-port ( gpio-pin -- in-port ) 2 cells + @ ;

: get-out-port ( gpio-pin -- out-port ) 1 cells + @ ;

: get-bit ( gpio-pin -- bit ) @ ;

: gpio-pin ( ddr in-port out-port bit -- ) create , , , , ;

: init-output-pin ( gpio-pin -- )
    cp>r get-bit 1 swap lshift r> get-ddr-port mset ;

: pin-high ( gpio-pin -- )
    cp>r get-bit 1 swap lshift r> get-out-port mset ;

: pin-low ( gpio-pin -- )
    cp>r get-bit 1 swap lshift r> get-out-port mclr ;
