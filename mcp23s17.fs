\ **********************************************************************
\
\ mcp23s17.fs is part of the ff-mcp23s17 project
\ ff-mcp23s17 is a project to intract with the MCP23S17 GPIO expander
\ using FlashForth
\ Copyright (C) 2021  Christopher Howard
\
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ This program is free software: you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation, either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <https://www.gnu.org/licenses/>.
\
\ This module provides register and bit definitions for the MCP23S17
\ as well as a word for initializing SPI appropriately for MCP23S17
\
\ public words:
\   B0_IODIRA B0_IODIRB B0_IPOLA B0_IPOLB B0_GPINTENA B0_GPINTENB
\   B0_DEFVALA B0_DEFVALB B0_INTCONA B0_INTCONB B0_IOCON B0_IOCON2
\   B0_GPPUA B0_GPPUB B0_INTFA B0_INTFB B0_INTCAPA B0_INTCAPB B0_GPIOA
\   B0_GPIOB B0_OLATA B0_OLATB B1_IODIRA B1_IODIRB B1_IPOLA B1_IPOLB
\   B1_GPINTENA B1_GPINTENB B1_DEFVALA B1_DEFVALB B1_INTCONA B1_INTCONB
\   B1_IOCON B1_IOCON2 B1_GPPUA B1_GPPUB B1_INTFA B1_INTFB B1_INTCAPA
\   B1_INTCAPB B1_GPIOA B1_GPIOB B1_OLATA B1_OLATB BANK_BT MIRROR_BT
\   SEQOP_BT DISSLW_BT HAEN_BT ODR_BT INTPOL_BT WRITE_BYTE READ_BYTE
\   init-spi
\
\ **********************************************************************

mcp23s17
marker mcp23s17

\ **********************************************************************
\ The register addresses. These change depending on whether we are in
\ IOCON.BANK=0 or IOCON.BANK=1 mode. BANK=0 is the default.
\ **********************************************************************

$00 constant B0_IODIRA
$01 constant B0_IODIRB
$02 constant B0_IPOLA
$03 constant B0_IPOLB
$04 constant B0_GPINTENA
$05 constant B0_GPINTENB
$06 constant B0_DEFVALA
$07 constant B0_DEFVALB
$08 constant B0_INTCONA
$09 constant B0_INTCONB
$0a constant B0_IOCON
$0b constant B0_IOCON2
$0c constant B0_GPPUA
$0d constant B0_GPPUB
$0e constant B0_INTFA
$0f constant B0_INTFB
$10 constant B0_INTCAPA
$11 constant B0_INTCAPB
$12 constant B0_GPIOA
$13 constant B0_GPIOB
$14 constant B0_OLATA
$15 constant B0_OLATB

$00 constant B1_IODIRA
$10 constant B1_IODIRB
$01 constant B1_IPOLA
$11 constant B1_IPOLB
$02 constant B1_GPINTENA
$12 constant B1_GPINTENB
$03 constant B1_DEFVALA
$13 constant B1_DEFVALB
$04 constant B1_INTCONA
$14 constant B1_INTCONB
$05 constant B1_IOCON
$15 constant B1_IOCON2
$06 constant B1_GPPUA
$16 constant B1_GPPUB
$07 constant B1_INTFA
$17 constant B1_INTFB
$08 constant B1_INTCAPA
$18 constant B1_INTCAPB
$09 constant B1_GPIOA
$19 constant B1_GPIOB
$0a constant B1_OLATA
$1a constant B1_OLATB

\ **********************************************************************
\ IOCON bits. Bit 0 reserved.
\ **********************************************************************

#7 constant BANK_BT
#6 constant MIRROR_BT
#5 constant SEQOP_BT
#4 constant DISSLW_BT
#3 constant HAEN_BT
#2 constant ODR_BT
#1 constant INTPOL_BT

\ **********************************************************************
\ If using hardware address bits, this need to be or'd with a byte of
\ format %0000AAA0 wth AAA being A2, A1, A0 address bits.
\ **********************************************************************

%01000000 constant WRITE_BYTE
%01000001 constant READ_BYTE

: init-spi ( -- )
    1 DD_MOSI   lshift
    1 DD_SCK    lshift or
    1 DD_SS     lshift or DDR_SPI mset
    0           SPR0 lshift
    0           SPR1 lshift or
    CPHA_SMPLED CPHA lshift or
    CPOL_LIDLE  CPOL lshift or
    MSTR_MSTR   MSTR lshift or
    DORD_MSB    DORD lshift or
    SPE_ENAB    SPE  lshift or
    SPIE_DISAB  SPIE lshift or SPCR c!
;
